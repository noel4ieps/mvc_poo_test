<?php
/*
  ./app/routeurs/categoriesRouteur.php
 */

$ctrl = new \App\Controleurs\CategoriesControleur();

 switch ($_GET['categories']):
   case 'show':
       $ctrl->showAction([
         'findOneBy'   => 'slug',
         'data'        => $_GET['slug']
       ]);
     break;

   default:
      die("Aucune route des catégories ne correspond.");
     break;
 endswitch;
