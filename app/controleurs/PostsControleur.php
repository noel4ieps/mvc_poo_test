<?php
/*
./app/controleurs/PostsControleur.php
 */

namespace App\Controleurs;

class PostsControleur extends \Noyau\Classes\ControleurGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_gestionnaire = new \App\Modeles\PostsGestionnaire();
  }

  public function indexByCategorieAction(int $id = null){
    // $ctrl->indexByCategorieAction($categorie->getId());
    $posts = $this->_gestionnaire->findAllByCategorie($id);
    include '../app/vues/posts/liste.php';
  }

}
