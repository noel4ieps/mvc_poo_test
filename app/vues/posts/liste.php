<?php
/*
./app/vues/posts/index.php
Variables disponibles :
    - $posts ARRAY(Post)
 */
?>
<?php foreach ($posts as $post): ?>

     <li>
       <a href="posts/<?php echo $post->getId(); ?>/<?php echo $post->getSlug(); ?>">
         <?php echo $post->getTitre(); ?>
       </a>
     </li>

<?php endforeach; ?>
