<?php
/*
./app/vues/catagories/index.php
Variables disponibles :
    - $categorie ARRAY(Categorie)
 */
?>
<?php
  GLOBAL $content1, $title;
  $title = $categorie->getTitre();
  ob_start();
?>

<h2>Catégorie: <?php echo $categorie->getTitre(); ?></h2>
<?php
  $ctrl = new \App\Controleurs\PostsControleur();
  $ctrl->indexByCategorieAction($categorie->getId());
?>

<?php
  $content1 = ob_get_clean();
?>
