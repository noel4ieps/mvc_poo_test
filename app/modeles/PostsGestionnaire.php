<?php
/*
./app/modeles/PostsGestionnaire.php
 */

namespace App\Modeles;
use \Noyau\Classes\App;

class PostsGestionnaire extends \Noyau\Classes\GestionnaireGenerique {

  public function __construct(){
    $this->_table = 'posts';
    $this->_modele = '\App\Modeles\Post';
  }

  public function findOneBySlug(string $slug){
    $sql = "SELECT *
            FROM posts
            WHERE slug = :slug;";
    $rs = App::getConnexion()->prepare($sql);
    $rs->bindValue(':slug', $slug, \PDO::PARAM_STR);
    $rs->execute();

    return new Post($rs->fetch(\PDO::FETCH_ASSOC));
  }

  public function findAllByCategorie(int $id){
    $sql = "SELECT *
            FROM posts
            JOIN posts_has_categories ON posts.id = post
            WHERE categorie = :id;";
    $rs = App::getConnexion()->prepare($sql);
    $rs->bindValue(':id',$id,\PDO::PARAM_INT);
    $rs->execute();
    return $this->convertPDOStatementToArrayObj($rs);
  }


//     $posts = $this->_gestionnaire->findAllByCategorie($id);



}
